import { Injectable }       from "@angular/core"
import { Http }             from "@angular/http";

import { Observable }       from "rxjs/Observable";

import 'rxjs/add/observable/throw';
import "rxjs/add/operator/map";

@Injectable()
export class UserService {
    constructor(
        private http: Http
    ) {}

    getUser(name): Observable<any> {
        return this.http.get(`/api/user/${name}`)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    errorHandler(err, caught): Observable<any> {
        console.log(err);
        return Observable.throw(new Error(err.status));
    }
}