import { Component,
         OnInit  }          from "@angular/core";

import { Router,
         ActivatedRoute,
         ParamMap }         from "@angular/router"; 

import { UserService }      from "./user.service";
import { TopicService }     from "../topic/topic.service";
import { User,
         Topic }            from "../custom-types";

import { Observable }       from "rxjs/Observable";
@Component({
    templateUrl: "./user.component.html",
    styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {
    user: User;
    saved: Observable<Topic[]>;
    submitted: Observable<Topic[]>;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private service: UserService,
        private topicService: TopicService
    ) {}
    
    ngOnInit(): void {        
        this.route.data
            .subscribe(res =>  this.user = res.user); 
        // Error is handled by resolver

        this.submitted = this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.topicService.getUserSubmitted(params.get("name")));
        
        this.saved = this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.topicService.getSaved(params.get("name")));
    }


    submittedUpdate() {
        this.submitted = this.submitted
            .switchMap(v => 
                this.topicService.getUserSubmitted(
                    this.user.name,
                    v[v.length - 1].datetime
                ));
    }

    savedUpdate() {
        this.saved = this.saved
            .switchMap(v => 
                this.topicService.getSaved(
                    this.user.name,
                    v[v.length - 1].datetime,
                ));
    }
}