import { Injectable }               from "@angular/core";
import { ActivatedRouteSnapshot, 
         Router, 
         Resolve, 
         RouterStateSnapshot }      from "@angular/router"; 

import { UserService }              from "./user.service";
import { User }                     from "../custom-types";

import { Observable }               from "rxjs/Observable";
import 'rxjs/add/operator/switchMap';

@Injectable()
export class UserResolver implements Resolve<User> {
    constructor(
        private service: UserService,
        private router: Router
    ) {}

    resolve(route: ActivatedRouteSnapshot, state:  RouterStateSnapshot): Observable<User> {
        let name = route.paramMap.get("name");

        return this.service.getUser("admin")
            .map(data => data)
            .catch((err, caught): Observable<any> => {
                this.router.navigate([""]);
                return Observable.of("");                
            });
    }
}