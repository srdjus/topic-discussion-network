import { NgModule }                 from "@angular/core";
import { CommonModule }             from "@angular/common";

import { AppRoutingModule }         from "../app-routing.module";

import { UserComponent }            from "./user.component";

import { UserService }              from "./user.service";

import { UserResolver }             from "./user.resolver";

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule
    ],
    declarations: [
        UserComponent
    ],
    providers: [
        UserService,
        UserResolver
    ]
})
export class UserModule { }