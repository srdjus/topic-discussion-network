import { NgModule }                 from "@angular/core";
import { CommonModule }             from "@angular/common";
import { FormsModule, 
         ReactiveFormsModule }      from "@angular/forms";
import { AppRoutingModule }         from "../app-routing.module";

import { LoginComponent }           from "./login.component";
import { JoinComponent }            from "./join.component";

import { SessionService }           from "./session.service";

import { SessionGuard }             from "./session.guard";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule
    ],
    declarations: [
        LoginComponent,
        JoinComponent
    ],
    providers: [
        SessionService,
        SessionGuard
    ]
})
export class SessionModule {}