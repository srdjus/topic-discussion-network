import { Component, 
         OnInit }           from "@angular/core";
import { FormGroup, 
         FormControl, 
         Validators }       from "@angular/forms";
import { Router }           from "@angular/router";

import { NewUser }          from "../custom-types";

import { SessionService }   from "./session.service";

import { Subject }          from "rxjs/Subject";
import { Observable }       from "rxjs/Observable";

import "rxjs/add/observable/merge";

@Component({
    templateUrl: "./join.component.html",
    styleUrls: ["./join.component.css"]
})
export class JoinComponent implements OnInit {
    message: string;

    // To be able to show messages depending on uniqeuness
    nameUnique: Boolean = false;
    emailUnique: Boolean = false;

    user: NewUser = new NewUser();
    userForm: FormGroup;

    // On name and email change we push data to stream

    private nameSubject = new Subject<Object>(); 
    private emailSubject = new Subject<Object>();

    nameChanged(value) {
        this.nameSubject.next({ name: value });
    }

    emailChanged(value) {
        this.emailSubject.next({ email: value });
    }

    constructor(
        private router: Router,
        private service: SessionService
    ) {}
    
    
    ngOnInit(): void {
        this.userForm = new FormGroup({
            'name': new FormControl(this.user.name, [
                Validators.minLength(2),
                Validators.maxLength(30),
                Validators.pattern("[\\w.!?_-]+")
            ]),
            'email': new FormControl(this.user.email, [
                Validators.email
            ]),
            'password': new FormControl(this.user.password, [
                Validators.minLength(5),
                Validators.pattern("[\\S]+")
            ])
        });

        Observable.merge(this.nameSubject, this.emailSubject)
            .debounceTime(500)
            .distinctUntilChanged()
            .switchMap(pair => this.service.testUniqueness(pair))
            .subscribe(res => {
                if (res.field === "email") 
                    if (res.unique) this.emailUnique = true;
                    else this.emailUnique = false; 
                
                else if (res.field === "name") 
                    if (res.unique) this.nameUnique = true;
                    else this.nameUnique = false; 
            }, err => {
                console.error(err);
            });
    }

    onSubmit(form, formObject) {
        this.service.join(form.value)
            .subscribe(res => {
                // Unlikely to happen because client is checking for
                // uniquness, but still need to handle this case 
                if (res.duplicate) {
                    this.message = `E-mail or name not unique. 
                                        Please try again!`;
                    return;
                }

                localStorage.setItem("kento", res.token);
                this.service.setSession(res.user)
                    .then(session => { 
                        this.service.confirmLogin(session);
                        // Create welcome page later, with guide
                        this.router.navigate([""]);
                    });
                        
            }, err => {
                console.error(err);
            });
    }

    get name() { return this.userForm.get('name'); }
    get email() { return this.userForm.get('email'); }
}