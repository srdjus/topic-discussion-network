import { Injectable }           from "@angular/core";
import { Http, 
         RequestOptions, 
         URLSearchParams }      from "@angular/http";

// Observables
import { Subject }              from "rxjs/Subject";
import { Observable }           from "rxjs/Observable";

import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";

import { Session, 
         User }                 from "../custom-types";

@Injectable()
export class SessionService {    
    private sessionInfo: Session;

    // String source
    private loginConfirmedSource = new Subject<Session>();
    
    // Observable stream
    loginConfirmed$ = this.loginConfirmedSource.asObservable();

    constructor(private http: Http) {}

    login(creds): Observable<any>  {
        return this.http.post(`/api/session/login`, creds)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    logout(): Observable<any> {
        return this.http.post(`/api/session/logout`, {})
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    // Checks session (default one)
    checkSessionUser(): Observable<Session> {
        return this.http.get(`/api/session/check-user`)
            .map(res => res.json().session)
            .catch(this.errorHandler);
    }
    
    // Same, but for moderation
    checkSessionModerator(): Observable<Session> {
        return this.http.get(`/api/session/check-moderator`)
            .map(res => res.json().session)
            .catch(this.errorHandler);
    }

    // Checks token stored in a browser
    verifySession(token): Observable<any> {
        return this.http.post('/api/session/verify-token', { token })
            .map(res => res.json())
            .catch(this.errorHandler);
}

    setSession(data): Promise<Session> {
        this.sessionInfo = data; 
        return Promise.resolve(this.sessionInfo);
    }

    getSession(): Promise<Session> {
        return Promise.resolve(this.sessionInfo);
    }
    
    // Manually pushing data to observable stream (with Subject)
    // after we get a answer from server that login is successfully done
    confirmLogin(session: Session) {
        this.loginConfirmedSource.next(session);
        this.loginConfirmedSource.complete();
    }

    join(user: User): Observable<any> {
        return this.http.post(`/api/session/join`, user)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    testUniqueness(pair): Observable<any> {
        let options = this.optionParams(pair);

        return this.http.get(`/api/session/test-unique`, options)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    errorHandler(err, caught): Observable<any> {
        return Observable.throw(new Error(err.status));
    }

    optionParams(pairs): RequestOptions {
        let params = new URLSearchParams();

        for (let i = 0; i < Object.keys(pairs).length; i++) {
            params.set(Object.keys(pairs)[i], pairs[Object.keys(pairs)[i]]);
        }
        
        return new RequestOptions({
            search: params 
        });
    }
}
