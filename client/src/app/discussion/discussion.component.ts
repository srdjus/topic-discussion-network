import { Component }            from "@angular/core";
import { ActivatedRoute, 
         ParamMap }             from "@angular/router"; 
import { Location }             from "@angular/common";

import { Reply, 
         Session }              from "../custom-types";

import { DiscussionService }    from "./discussion.service";
import { SessionService }       from "../session/session.service";

import { Observable }           from "rxjs/Observable";

@Component({
    selector: "comments",
    templateUrl: "./discussion.component.html",
    styleUrls: ["./discussion.component.css"]
})
export class DiscussionComponent {
    title: string = "Discussion on ..."
    session: Observable<Session>;

    id: string; 
    bookId: string;
    
    replies: Reply[];
    newReply: Reply = new Reply(); 
    currentPage: number = 1;
    replyCount: number; 

    constructor(
        private route: ActivatedRoute, 
        private location: Location,
        private service: DiscussionService,
        private sessionService: SessionService
    ) {}

    ngOnInit() {
        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.getReplies(params.get('id'), null))
                .subscribe(res => {
                    this.prettyDate(res);
                    this.replies = res;
                }, err => {
                    console.error(err);
                });

        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.countReplies(params.get('id')))
                .subscribe(res => { 
                    this.replyCount = parseInt(res);
                }, err => {
                    console.error(err);
                });

        this.session = this.sessionService.checkSessionUser();
    }

    showMore() {
        this.currentPage++;
        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.getReplies(params.get('id'), { 
                    page: this.currentPage, 
                    last: this.replies[this.replies.length - 1].datetime
                }))
            .subscribe(res => {
                this.prettyDate(res);
                this.replies = [...this.replies, ...res];
            }, err => {
                console.error(err);
            });
    }

    postReply() {
        this.route.paramMap 
            .switchMap((params: ParamMap) => {
                /* Also sending the datetime of the newest one so we can pull 
                the new ones from database along with the one we inserted */
                return this.service.reply(params.get('id'), {
                    newest: this.replies[0] ? this.replies[0].datetime : 0,
                    reply: this.newReply
                });
            })
            .subscribe(update => {
                update = update.sort((a, b) => 
                    a.datetime - b.datetime <= 0);

                this.prettyDate(update);
                    
                // Put newest ones at the beginning
                this.replies = [...update, ...this.replies];
                this.replyCount += update.length; 
                // Reset new reply with constructor
                this.newReply = new Reply();
            }, err => {
                console.error(err);
            });
    }

    // Attaching pretty date string to every item in the array
    prettyDate(replies) {
        let options = {
            year: 'numeric', month: 'long', day: 'numeric',
            hour: 'numeric', minute: 'numeric',
            hour12: false
        };

        replies.map(reply => {
            let ugly = new Date(reply.datetime);
            reply.datetimePretty = new Intl
                .DateTimeFormat('en-US', options)
                .format(ugly);
        });
    }
}
