import { NgModule }                 from "@angular/core";
import { CommonModule }             from "@angular/common";
import { FormsModule }              from '@angular/forms';
import { AppRoutingModule }         from "../app-routing.module";

import { DiscussionComponent }      from "./discussion.component";

import { DiscussionService }        from "./discussion.service";
import { SessionService }           from "../session/session.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AppRoutingModule
    ],
    declarations: [
        DiscussionComponent
    ],
    providers: [
        DiscussionService,
        SessionService
    ]
})
export class DiscussionModule { }
