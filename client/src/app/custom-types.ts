export class Topic {
    title: string; 
    _id: string;
    author: string;
    category: string;
    year: number;
    imgUrl: string; 
    description: string;
    genre: string;
    datetime: number;
}

export class Reply {
    name: string; 
    content: string;
    datetime: string;
}

export class Credentials {
    username: string; 
    password: string;
}

export class Session {
    _id: string;
    name: string; 
    priority: number;
    pie: string; 
}

export class NewUser {
    name: string;
    email: string;
    password: string;
}

export class User {
    _id: string;
    name: string;
    saved: Object[];
}