import { Component,
         OnInit,
         Input }                from "@angular/core";

import { Observable }           from 'rxjs/Observable';
import { Subject }              from 'rxjs/Subject';
         
import { TopicService }         from "../topic/topic.service";
import { SessionService }       from "../session/session.service";

import { Topic, 
         Session }              from "../custom-types";

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: "top-menu",
    templateUrl: "./menu.component.html",
    styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit {
    @Input() session: Session;

    found: Topic[]; 
     
    private searchTerms = new Subject<string>();

    constructor(
        private topicService: TopicService,
        private sessionService: SessionService
    ) {}

    ngOnInit(): void {
        this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term =>  
                term ? this.topicService.searchTopic(term) 
                     : Observable.of<Topic[]>([]))
            .subscribe(topics => this.found = topics);
    }

    // Push the value into observable stream
    search(term: string): void {
        this.searchTerms.next(term);
    }

    logoutProceed() {
        this.sessionService
            .logout()
            .subscribe(res => { 
                if (res.ok) {
                    this.session = null; 
                    localStorage.removeItem("kento");
                    location.reload();
                }
            }, err => console.error(err));
    }
}