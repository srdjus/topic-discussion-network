import { Component, 
         Input }                from "@angular/core";

import { TopicService }         from "../topic/topic.service"; 

import { Session }              from "../custom-types";

@Component({
    selector: "sidebar",
    templateUrl: "./sidebar.component.html", 
    styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent {
    @Input() present: boolean; // Toggling search bar
    @Input() session: Session; 
}
