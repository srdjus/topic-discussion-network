import { NgModule }                 from "@angular/core";
import { CommonModule  }            from "@angular/common";
import { AppRoutingModule }         from "../app-routing.module";

import { DashboardComponent }       from "./dashboard.component";
import { MenuComponent }            from "./menu.component";
import { SidebarComponent }         from "./sidebar.component";
import { PageNotFoundComponent }    from "./pagenotfound.component";

import { TopicService }             from "../topic/topic.service";
import { SessionService }           from "../session/session.service";

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule
    ],
    declarations: [
        DashboardComponent,
        MenuComponent,
        SidebarComponent,
        PageNotFoundComponent
    ],
    exports: [
        DashboardComponent,
        MenuComponent,
        SidebarComponent,
    ],
    providers: [
        TopicService,
        SessionService
    ]
})
export class SharedModule { }
