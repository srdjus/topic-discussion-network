import { BrowserModule }            from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { HttpModule }               from "@angular/http";

import { TopicModule }              from "./topic/topic.module";
import { SessionModule }            from "./session/session.module";
import { SharedModule }             from "./shared/shared.module";
import { DiscussionModule }         from "./discussion/discussion.module";
import { UserModule }               from "./user/user.module";
import { AppRoutingModule }         from "./app-routing.module";

import { AppComponent }             from "./app.component";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        TopicModule,
        SessionModule,
        SharedModule,
        UserModule,
        DiscussionModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
