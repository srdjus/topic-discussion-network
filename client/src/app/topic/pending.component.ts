import { Component, 
         OnInit }          from "@angular/core"; 
import { ActivatedRoute, 
         ParamMap }        from "@angular/router"; 

import { TopicService }    from "./topic.service";
import { Topic }           from "../custom-types";
import { SessionService }  from "../session/session.service";

@Component({
    templateUrl: "./pending.component.html", 
    styleUrls: ["./pending.component.css"]
})
export class PendingComponent implements OnInit {
    topics: Topic[];

    constructor(
        private service: TopicService,
        private sessionService: SessionService,
        private route: ActivatedRoute 
    ) {}

    ngOnInit() {
        this.route.queryParamMap
            .switchMap((params: ParamMap) => 
                this.service.getReviews(params.get("reviewed")))
            .subscribe(res => { 
                    this.topics = res; 
                }, err => { 
                    console.error(err);
                });
    }

    answer(id, value) {
        this.service.reviewed(id, value)
            .subscribe(() => {
                let i = this.topics.findIndex(o => o._id === id);
                this.topics.splice(i, 1);
            }, err => {
                console.error(err);
            });
    }
}
