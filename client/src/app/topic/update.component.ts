import { Component, 
         Input,
         OnInit }           from "@angular/core";

import { FormGroup, 
         FormControl, 
         Validators}        from "@angular/forms";

import { Router, 
         ActivatedRoute, 
         ParamMap }         from "@angular/router";

import { Location }         from "@angular/common";

import { TopicService }     from "./topic.service";
import { Topic }            from "../custom-types";

@Component({
    selector: "update",
    templateUrl: "./update.component.html",
    styleUrls: ["./update.component.css"]
})
export class UpdateComponent implements OnInit {
    @Input() topic: Topic;
    form: FormGroup;
    message: string;
    
    constructor(
        private service: TopicService,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        // Missing validation
        this.form = new FormGroup({
            'title': new FormControl(this.topic.title, [
                Validators.minLength(2), 
                Validators.maxLength(40),
                Validators.pattern("[\\w\\s\'\".,]+")
            ]), 
            'author': new FormControl(this.topic.author, [
                Validators.minLength(2), 
                Validators.maxLength(25),
                Validators.pattern("[\\w\\s\'.]+")
            ]), 
            'description': new FormControl(this.topic.description, [
                Validators.minLength(10), 
                Validators.maxLength(500)
            ]),
            'category': new FormControl(this.topic.category, [
                Validators.required
            ]),
            'imgUrl': new FormControl(this.topic.imgUrl),
            'year': new FormControl(this.topic.year), 
            'genre': new FormControl(this.topic.genre)
        });
    }

    update(formObject) {
        let updated = {};

        for (let key of Object.keys(this.form.controls))
            if (this.form.controls[key].dirty)
                updated[key] = this.form.controls[key].value;

        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.updateTopic(updated, params.get("id")))
            .subscribe(() => console.log("Updated!"), err => 
                 console.error(err)
            );

        // Show a message after successful/unsuccessful update
    }

    remove() {
        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.deleteTopic(params.get("id")))
            .subscribe(r =>  this.location.back(), err => 
                console.error(err)
            );
    }

    pending(val: Boolean) {
        this.route.paramMap
            .switchMap((params: ParamMap) => 
                this.service.reviewed(params.get("id"), val))
            .subscribe(res =>  
                this.router.navigate(["/topics/pending"]), err => 
                console.error(err)
            );
    }
}