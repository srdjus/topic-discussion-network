import { Injectable }               from "@angular/core";
import { ActivatedRouteSnapshot, 
         Router, 
         Resolve, 
         RouterStateSnapshot }      from "@angular/router"; 

import { TopicService }             from "./topic.service"; 
import { Topic }                    from "../custom-types";

import { Observable }               from "rxjs/Observable"; 

@Injectable()
export class TopicResolver implements Resolve<Topic> {
    constructor(
        private router: Router,
        private service: TopicService 
    ) {}

    resolve(route: ActivatedRouteSnapshot, state:  RouterStateSnapshot): Observable<Topic> {
        let id = route.paramMap.get('id');

        return this.service.getOneTopic(id)
            .map(data => data)
            .catch((err, caught): Observable<any> => {
                console.error(err);
                this.router.navigate([""]);
                // Just return something we navigated already
                return Observable.of('');                
            });
    }
}