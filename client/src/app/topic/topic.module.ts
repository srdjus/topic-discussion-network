import { NgModule }                 from "@angular/core";
import { CommonModule }             from "@angular/common";
import { FormsModule, 
         ReactiveFormsModule }      from "@angular/forms";
import { AppRoutingModule }         from "../app-routing.module";

import { TopicsComponent }          from "./topics.component";
import { TopicSingleComponent }     from "./topic-single.component";
import { NewTopicComponent }        from "./new-topic.component";
import { UpdateComponent }          from "./update.component";
import { PendingComponent }         from "./pending.component";

import { TopicService }             from "./topic.service";
import { SessionService }           from "../session/session.service";

@NgModule({
    imports: [ 
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule
    ],
    declarations: [
        TopicsComponent,
        TopicSingleComponent,
        NewTopicComponent,
        UpdateComponent,
        PendingComponent
    ],
    providers: [
        TopicService,
        SessionService
    ]
})
export class TopicModule {}
