import { Component }        from "@angular/core"; 
import { ActivatedRoute }   from "@angular/router";
import { Location }         from "@angular/common";

import { TopicService }     from "./topic.service";
import { SessionService }   from "../session/session.service";

import { Topic, 
         Session }          from "../custom-types";

import { Observable }       from "rxjs/Observable";``
import 'rxjs/add/operator/switchMap';

@Component({
    templateUrl: "./topic-single.component.html",
    styleUrls: ["./topic-single.component.css"]
})
export class TopicSingleComponent {
    topic: Topic; 

    session: Observable<Session>; 

    constructor(
        private route: ActivatedRoute,
        private service: TopicService,
        private sessionService: SessionService,
    ) {}

    ngOnInit() {
        // Pre-fetching data with resolver
        this.route.data
            .subscribe(res => this.topic = res.topic);
        
        this.session = this.sessionService.checkSessionModerator();
    }
}