import { Injectable }        from "@angular/core";
import { Http, 
         Headers, 
         RequestOptions, 
         URLSearchParams }   from "@angular/http";

import { Session, Topic }    from "../custom-types";

import { Observable }        from "rxjs/Observable";

import 'rxjs/add/observable/throw';
import "rxjs/add/operator/map";

@Injectable()
export class TopicService { 
    private header = new Headers(
        { "Content-Type": "application/json" }
    );

    constructor (private http: Http ) {}
    
    getByCategory(category): Observable<any> {
        return this.http.get(`/api/topics/category/${category}`)
            .map(res => res.json())
            .catch(this.errorHandler);
    }


    getOneTopic(id): Observable<any> {
        return this.http.get(`/api/topics/${id}`)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    getReviews(reviewed): Observable<any> {
        let options = this.optionParams({ 
            reviewed 
        });

        return this.http.get("/api/topics/review", options)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    reviewed(id, value): Observable<any> {
        return this.http.post("/api/topics/review", 
            JSON.stringify({ id, value }), { headers: this.header })
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    insertTopic(topic): Observable<any> {
        return this.http.post(`/api/topics`, 
            JSON.stringify(topic), { headers: this.header })
            .map(res => res.json())
            .catch(this.errorHandler);
        
    }

    updateTopic(update, id): Observable<any> {
        return this.http.put(`/api/topics/${id}`, 
            JSON.stringify(update), { headers: this.header })
            .map(res => res.json())
            .catch(this.errorHandler);
}

    deleteTopic(id): Observable<any> {
        return this.http.delete(`/api/topics/${id}`)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    searchTopic(value: string): Observable<Topic[]> {
        let options = this.optionParams({
            search: value
        });

        return this.http.get(`/api/topics`, options)
            .map(res => res.json() as Topic[])
            .catch(this.errorHandler);
    }

    getSaved(name: string, datetime?: number): Observable<any> {
        let options = this.optionParams({ name, datetime });
        
        return this.http.get(`/api/topics/saves`, options)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    saveTopic(id: string): Observable<any> {
        return this.http.post("/api/topics/saves", { id })
            .map(res => res.json())
            .catch(this.errorHandler);

    }

    removeSave(id: string): Observable<any> {
        let options = this.optionParams({ id });

        return this.http.delete("/api/topics/saves", options)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    getUserSubmitted(name: string, datetime?: number, type?: number): Observable<any> {
        let options = this.optionParams({ name, datetime });

        return this.http.get(`/api/topics/submitted`, options)
            .map(res => res.json())
            .catch(this.errorHandler);
    }

    optionParams(pairs): RequestOptions {
        let params = new URLSearchParams();

        for (let i = 0; i < Object.keys(pairs).length; i++)
            params.set(Object.keys(pairs)[i], pairs[Object.keys(pairs)[i]]);

        return new RequestOptions({
            search: params 
        });
    }

    errorHandler(err, caught): Observable<any> {
        return Observable.throw(new Error(err.status));
    }
}
