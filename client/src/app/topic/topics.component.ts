import { Component }            from "@angular/core";
import { Router, 
         ActivatedRoute, 
         ParamMap }             from "@angular/router";

import { TopicService }         from "./topic.service";
import { SessionService }       from "../session/session.service";
         
import { Topic, 
         Session }              from "../custom-types";

import { Observable }           from "rxjs/Observable";
         

@Component({
    selector: "topics", 
    templateUrl: "./topics.component.html",
    styleUrls: ["./topics.component.css"]
})
export class TopicsComponent {
    category: string;
    topics: Topic[];
    newTopic: Topic = new Topic();
    session: Observable<Session>;
    
    constructor(
        private router: Router,
        private service: TopicService,
        private sessionService: SessionService, 
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.route.queryParamMap
            .switchMap((params: ParamMap) => {
                this.category = params.get("category");
                return this.service
                    .getByCategory(this.category);
                })
            .subscribe(r => {
                /* Remove isSaved field if false, change
                to boolean otherwise */
                r.map(i => {
                    if (!i.isSaved) delete(i.isSaved);
                    else i.isSaved = !!i.isSaved.length;
                    return i;
                });

                this.topics = r 
            }, err => {
                console.error(err);
            });

        this.session = this.sessionService.checkSessionUser();
            
    }

    save(topic) {
        if (!topic.isSaved) {
            this.service.saveTopic(topic._id)
                .subscribe(() => {
                    topic.isSaved = true;
                    topic.timesSaved++;
                }, err => console.error(err));
        } else {
            this.service.removeSave(topic._id)
                .subscribe(() => {
                    topic.isSaved = false;
                    topic.timesSaved--;                    
                }, err => console.error(err));           
        }
    }
}
