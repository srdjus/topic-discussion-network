import { Component, OnInit }    from "@angular/core";
import { Router }               from "@angular/router";
import { Location }             from "@angular/common";

import { SessionService }       from "./session/session.service";
import { Session }              from "./custom-types";

import { Observable }           from 'rxjs/Observable';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [ SessionService ]
})
export class AppComponent implements OnInit {
    session: Session = null; 

    constructor( 
        private location: Location,
        private router: Router,
        private sessionService: SessionService) {
            // Value will be emitted on user login success
            sessionService.loginConfirmed$
                .subscribe(session => this.session = session);
    }

    ngOnInit() {
        // TODO: Session should be checked before route is activated (with resolver)
        if (localStorage.getItem("kento")) 
            this.sessionService
                .verifySession(localStorage.getItem("kento"))
                .subscribe(res => {
                    if (res.valid) {
                        this.sessionService.setSession(res.user)
                            .then(session => this.session = session)
                    } else {
                        localStorage.removeItem("kento");
                    }
                }, err => console.error(err));
    }
    
    goBack() {
        this.location.back();
    }
}