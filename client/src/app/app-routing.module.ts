import { NgModule }                 from "@angular/core";
import { RouterModule, 
         Routes }                   from "@angular/router";

import { TopicsComponent }          from "./topic/topics.component";
import { TopicSingleComponent }     from "./topic//topic-single.component";
import { NewTopicComponent }        from "./topic/new-topic.component";
import { PendingComponent }         from "./topic/pending.component";
import { DiscussionComponent }      from "./discussion/discussion.component";
import { DashboardComponent }       from "./shared/dashboard.component";
import { LoginComponent }           from "./session/login.component";
import { JoinComponent }            from "./session/join.component";
import { UserComponent }            from "./user/user.component";
import { PageNotFoundComponent }    from "./shared/pagenotfound.component"; 

import { SessionGuard }             from "./session/session.guard";

import { TopicResolver }            from "./topic/topic.resolver";
import { UserResolver }             from "./user/user.resolver";
 
const appRoutes: Routes = [ 
    {
        path: 'topics', 
        component: TopicsComponent 
    },
    {
        path: 'topic/:id',
        component: TopicSingleComponent,
        resolve: { topic: TopicResolver }
    },
    {
        path: 'topic/:id/discussion',
        component: DiscussionComponent
    },
    {
        path: 'topics/create', 
        component: NewTopicComponent,
        canActivate: [SessionGuard]
    },
    {
        path: "topics/pending", 
        component: PendingComponent,
        canActivate: [SessionGuard]
    },
    {
        path: "dashboard", 
        component: DashboardComponent
    },
    {
        path: "login", 
        component: LoginComponent,
        canActivate: [SessionGuard]
    },
    {
        path: "join",
        component: JoinComponent,
        canActivate: [SessionGuard]
    },
    {
        path: "user/:name",
        component: UserComponent,
        resolve: { user: UserResolver }
    },
    {
        path: '',
        redirectTo: '/dashboard', 
        pathMatch: "full"
    }, 
    {
        path: "**",
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    providers: [SessionGuard, TopicResolver, UserResolver]
})
export class AppRoutingModule { }
