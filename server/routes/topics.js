const router = require("express").Router();
const assert = require("assert");
const ObjectID = require("mongodb").ObjectID;

const topicModel = require("../models/topic.model.js");
const sessionMW = require("../middlewares/session.middleware");
const sharedMW = require("../middlewares/shared.middleware");
const validator = require("../validators/topics.validator");

router.get("/", (req, res, next) => {
    topicModel.get(req.query.search)
        .then(titles => res.json(titles))
        .catch(err => next(err));
});

router.post("/", (req, res, next) => {
    topicModel.insert(req.body, req.session.user)
        .then(r => {  
            /* TODO: unreviewed topic should be visible to admins, mods
            and also users who created it (with specific message) */
            if (req.session.user.priority < 3) {
                res.json({ 
                    _id: r.insertedId, 
                    navigate: true 
                });
            } else { 
                res.json({ _id: r.insertedId });
            }
        })
        .catch(err => next(err));
});

// Get by category
router.get("/category/:category", (req, res, next) => {
    topicModel.getByCategory(req.params.category, req.session.user)
        .then(titles => res.json(titles))
        .catch(err => next(err));
});

// Pending and rejected topics (for mods)
router.post("/review", sessionMW.mod, (req, res, next) => {
    topicModel.setReview(req.body.id, req.body.value)
        .then(() => res.json({ ok: true }))
        .catch(err => next(err));
});

router.get("/review", sessionMW.mod, (req, res, next) => {    
    topicModel.getReviews(req.query.reviewed)
        .then(data => res.json(data))
        .catch(err => next(err));
});

// Topics saved by single user
router.get("/saves", (req, res, next) => {
    topicModel.getSaved(req.query)
        .then(data => res.json(data))
        .catch(err => next(err));
});

router.post("/saves", (req, res, next) => {
    if (!req.session.user) {
        return next(new Error('401'));
    }
    
    topicModel.insertSave(req.body, req.session.user)
        .then(() => res.json({ ok: true }))
        .catch(err => next(err));
});

router.delete("/saves", (req, res, next) => {
    topicModel.removeSave(req.query, req.session.user)
        .then(() => res.json({ ok: true }))
        .catch(err => next(err));
});

// Submitted topics by user 

router.get("/submitted", (req, res, next) => {
    topicModel.getSubmitted(req.query)
        .then(topics => res.json(topics))
        .catch(err => next(err));
});

// Single topic
router.get("/:id", (req, res, next) => {
    topicModel.getOne(req.params.id, req.session.user)
        .then(title => res.json(title))
        .catch(err => next(err));
});

router.delete("/:id", sessionMW.mod, (req, res, next) => {
    topicModel.delete(req.params.id)
        .then(() => res.json({ ok: true }))
        .catch(err => next(err));
});

router.put("/:id", sessionMW.mod, (req, res, next) => {
    topicModel.update(req.params.id, req.body)
        .then(() => res.json({ ok: true }))
        .catch(err => next(err));
});

router.get("/:id/count", (req, res, next) => {
    topicModel.countReplies(req.params.id)
        .then(r => res.json({ count: r }))
        .catch(err => next(err));
});

// Discussion
router.post("/:id/discussion", (req, res, next) => {
    topicModel.insertReply(req.body, req.session.user, req.params.id)
        .then(r => res.json(r))
        .catch(err => next(err));
});

router.get("/:id/discussion", (req, res, next) => {
    topicModel.getReplies(req.params.id, req.query)
        .then(docs => res.json(docs))
        .catch(err => next(err));
});

// Error handling middleware
router.use(sharedMW.errorHandler);

module.exports = router;