const router = require("express").Router();

const sharedMW = require("../middlewares/shared.middleware");
const userModel = require("../models/user.model");

router.get("/:name", (req, res, next) => {
    userModel.getOne(req.params.name)
        .then(user => res.json(user))
        .catch(err => next(err));
});

// Error handling middleware
router.use(sharedMW.errorHandler);

module.exports = router;