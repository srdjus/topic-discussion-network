const router = require("express").Router();
const jwt = require("jsonwebtoken");
const assert = require("assert");

const sessionModel = require("../models/session.model");
const sharedMW = require("../middlewares/shared.middleware");
const sessionHelper = require("../helpers/session.helper");

// Date-time middleware 
router.use(sharedMW.datetime);

router.post("/verify-token", (req, res, next) => {
    sessionHelper.tokenVerify(req.body.token)
        .then(id => sessionModel.find(id))
        .then(data => {
            if (!req.session.user)
                req.session.user = data; 
            
            delete(data["password"]);

            res.json({
                valid: true, 
                user: data
            });
        }).catch(err => res.json({ valid: false }));
});

router.get("/check-user", (req, res, next) => {
    res.json({ 
        session: req.session.user
            ? req.session.user : null 
    });
});

router.get("/check-moderator", (req, res, next) => {
    res.json({ 
        session: req.session.user && req.session.user.priority < 3
            ? req.session.user : null 
    });
});

router.post("/login", (req, res, next) => {
    sessionModel.loginFind(req.body)
        .then(data => {
            if (data && data.name === req.body.password) {
                req.session.user = data;
                
                let token = sessionHelper.tokenSign(data._id);

                delete(data["password"]);

                res.json({ 
                    ok: true, 
                    user: data, 
                    token: token
                });
            } else { 
                res.json({ ok: false });
            }
        })
        .catch(err => next(err));
});

router.post("/logout", (req, res, next) => {
    req.session.destroy(err => { 
        if (err) 
            return next(err);
            
        res.send({ ok: true });
    }); 
});

router.post("/join", (req, res, next) => {
    sessionModel.newUser(req.body)
        .then(r => {   
            // Setting up a session for new user
            req.session.user = r.session;

            // Sending session and token
            res.json({ 
                user: req.session.user,
                token: r.token
            });
        })
        .catch(err => { 
            // Extract the "duplicate key error" from other errors
            if (err.code === 11000)
                return res.json({ duplicate: true });
                   
            next(err);
        });
});

router.get("/test-unique", (req, res, next) => {
    sessionModel.isUnique(req.query)
        .then(r => res.json(r))
        .catch(err => next(err));
});

// Error handling middleware
router.use(sharedMW.errorHandler);

module.exports = router;