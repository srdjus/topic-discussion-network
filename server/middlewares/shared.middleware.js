module.exports.errorHandler = (err, req, res, next) => {
    console.log(err);

    if (err.message === '400')
        res.status(400).send("Well, that was weird,."); 
    
    else if (err.message === '401')
        res.status(401).send("You can't do that.");

    else if (err.message === '404')
        res.status(404).send("Oops, can't find that.")

    else
        res.status(500).send("Internal error.");
};

module.exports.datetime = (req, res, next) => {
    req.datetime = Date.now();
    next();
}