module.exports.mod = (req, res, next) => {
    if (req.session.user && req.session.user.priority < 3) {
        next();
    } else { 
        res.status(401).send("You can't do that");
    }
}