const getdb = require("../db.js");

let db = null;
getdb.then(database => db = database);

module.exports.getOne = (name) => {
    // TODO: Validate name
    
    return db.collection("users")
        .findOne({ name: name}, { password: 0, email: 0 })
        .then(data => {
            if (!data)
                return Promise.reject(new Error("404"));
            return Promise.resolve(data);
        });
}