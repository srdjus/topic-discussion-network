const ObjectID = require("mongodb").ObjectID;
const getdb = require("../db");

const validator = require("../validators/topics.validator");

let db = null; 
getdb.then(database => db = database); 

// Used only for search
module.exports.get = (search) => {
    if (!validator.doSearch(search))
        return Promise.resolve([]);
  
    return db.collection("titles")
        .find({
            title: {
                $regex: search,
                $options: 'i'
            },
            passed: true
        }, {
            title: 1,
            category: 1
        })
        .limit(10)
        .toArray();
};

module.exports.getByCategory = (category, user) => {
    if (!validator.doOptional(category)) {
        return Promise.reject(new Error('400'));
    }

    let isSaved = null;

    // If user is logged in check if he saved specific topic 
    if (user) {
        isSaved = { 
            $filter: {
                input: "$saves",
                as: "save",
                cond: {
                    $eq: ["$$save.user_name", user.name]
                } 
            }
        };
    }

    return db.collection("titles").aggregate([
        {
            $match: {
                category,
                passed: true
            }
        },
        { 
            $lookup: { 
                from: "replies", 
                localField: "_id", 
                foreignField: "title_id", 
                as: "replies" 
            }
        },
        { 
            $lookup: { 
                from: "saves", 
                localField: "_id", 
                foreignField: "topic_id", 
                as: "saves" 
            }
        }, 
        {
            $addFields: {
                repliesCount: { 
                    $size: "$replies"  
                },
                timesSaved: {
                    $size: "$saves"
                },
                isSaved: isSaved
            }
        },
        {
            $project: {
                saves: 0,
                replies: 0
            }
        }
    ])
    .toArray();
}

module.exports.getOne = (id, user) => {
    if(!ObjectID.isValid(id)) {
        return Promise.reject(new Error('404'));
    }

    let match = { _id: ObjectID(id) };
    
    // In case of normal users show approved topics
    if (!user || user.priority > 3) {
        match.passed = true;
    }

    return db.collection("titles").aggregate([
        {
            $match: match 
        }, 
        { 
            $lookup: { 
                from: "replies", 
                localField: "_id", 
                foreignField: "title_id", 
                as: "replies" 
            }
        }, 
        {
            $limit: 1
        }, 
        { 
            $lookup: { 
                from: "saves", 
                localField: "_id", 
                foreignField: "topic_id", 
                as: "saves" 
            }
        }, 
        { 
            $addFields: {
                repliesCount: { 
                    $size: "$replies"  
                },
                timesSaved: {
                    $size: "$saves"
                }
            }   
        }, 
        {
            $project: {
                replies: 0,
                saves: 0
            }
        }
    ])
    .toArray()
    .then(data => {
        if (!data.length) {
            return Promise.reject(new Error("404"));
        } else return Promise.resolve(data[0]);
    });
}

module.exports.insert = (topic, user) => {
    if (!user) {
        return Promise.reject(new Error("401"));
    }

    if (!validator.doPost(topic) || 
        !ObjectID.isValid(user._id)) {
            return Promise.reject(new Error("400"));
    }

    if (user.priority < 3) {
        topic.passed = topic.reviewed = true;
    } else { 
        topic.passed = topic.reviewed = false;
    }

    topic.user_name = user.name;
    topic.datetime = Date.now();
    
    return db.collection("titles")
        .insertOne(topic);
}

module.exports.update = (id, update) => {
    if (!ObjectID.isValid(id) || !validator.doPut(update)) {
        return Promise.reject(new Error('400'));    
    }

    return db.collection("titles")
        .update({ 
            _id: ObjectID(id) 
        }, {
             $set: update 
        });
}

module.exports.delete = (id) => {
    if(!ObjectID.isValid(id)) {
        return Promise.reject(new Error('400'));
    }

    return db.collection("titles")
        .deleteOne({ _id: ObjectID(id)});
}

// Saves
module.exports.getSaved = (params) => {
    let match = {
        user_name: params.name
    };

    return db.collection("saves").aggregate([
        {
            $lookup: {
                from: "titles", 
                localField: "topic_id",
                foreignField: "_id",
                as: "topics"
            }
        },
        {
            $match: match
        },
        {
            $sort: {
                "topics.datetime": 1
            }
        },
        {
            $project: {
                topic: {
                    $arrayElemAt: ["$topics", 0]
                }
            }
        },
        // Attaching replies and saves count to the topic
        {
            $lookup: {
                from: "saves",
                localField: "topic._id",
                foreignField: "topic_id",
                as: "topic.saves"
            }
        },
        {
            $lookup: {
                from: "replies",
                localField: "topic._id",
                foreignField: "title_id",
                as: "topic.replies"
            }
        },
        {
            $project: {
                topic: {
                    _id: 1,
                    title: 1,
                    description: 1,
                    savedCount: {
                        $size: "$topic.saves"
                    },
                    repliesCount: {
                        $size: "$topic.replies"
                    }
                }
            }
        },
        {
            $limit: 3
        }
    ])
    .toArray()
    .then(data => Promise.resolve(data.map(i => i.topic)));
}

module.exports.insertSave = (query, user) => {
    if (!ObjectID.isValid(query.id)) {
        return Promise.reject(new Error("Not valid ObjectID."));
    }

    let payload = {
        topic_id: ObjectID(query.id),
        user_name: user.name
    };

    return db.collection("saves")
        .insertOne(payload);
}

module.exports.removeSave = (query, user) => {
    if (!ObjectID.isValid(query.id)) {
        return Promise.reject(new Error("Not valid ObjectID."));
    }

    let payload = {
        topic_id: ObjectID(query.id),
        user_id: user.name
    };

    return db.collection("saves")
        .deleteOne(payload);
}

// Submitted 

module.exports.getSubmitted = (params) => {
    // TODO: Check name if it's valid

    let query = { 
        user_name: params.name
    };
    
    return db.collection("titles").aggregate([
        {
            $match: {
                user_name: params.name,
                passed: true
            }
        },
        {
            $limit: 3
        },
        {
            $lookup: {
                from: "saves",
                localField: "_id",
                foreignField: "topic_id",
                as: "saves"
            }
        },
        {
            $lookup: {
                from: "replies",
                localField: "_id",
                foreignField: "title_id",
                as: "replies"
            }
        },
        {
            $addFields: {
                savedCount: {
                    $size: "$saves"
                },
                repliesCount: {
                    $size: "$replies"
                }
            }
        },
        {
            $project: {
                saves: 0,
                replies: 0
            }
        }
    ])
    .toArray();
}

// Discussion
module.exports.getReplies = (topicId, info) => {
    if (!ObjectID.isValid(topicId)) {
        return Promise.reject(new Error('404'));
    }
    
    let query = { 
        title_id: ObjectID(topicId) 
    };

    if (Object.keys(info).length) {
        // info.last: timestamp of last showed reply
        if (!validator.doNumbers(info.last))
            return Promise.reject(new Error('400'));

        query.datetime = {
            $lt: parseInt(info.last)
        };
    }

    return db.collection("replies")
        .find(query)
        .sort({ datetime: -1 })
        
        .limit(10)
        .toArray();
}

module.exports.insertReply = (body, user, topicId) => {
    if (!user) {
        return Promise.reject(new Error('401'));
    }

    let {newest, reply} = body;

    if (!validator.doReply(reply) || 
        !ObjectID.isValid(topicId)) {
            return Promise.reject(new Error('400'));
    }

    reply.title_id = ObjectID(topicId);
    reply.datetime = Date.now();
    reply.name = user.name;

    return db.collection("replies")
        .insertOne(reply)
        // TODO: Update to get new replies including this one
        .then((inserted) => {
            if (!newest) {
                return Promise.resolve(inserted.ops);
            }

            let query = {
                title_id: ObjectID(topicId), 
                datetime: {
                    $gt: parseInt(newest)
                }
            };

            return db.collection("replies")
                .find(query)
                .toArray();
        });
}

module.exports.countReplies = (id) => {
    if(!ObjectID.isValid(id)) {
        return Promise.reject(new Error('400'));
    }

    return db.collection("replies")
        .count({ title_id: ObjectID(id) });
}

// Review
module.exports.getReviews = (reviewed) => {
    let query = {}; 
    
    if (reviewed) {
        // This will fetch rejected topics
        query.reviewed = true; 
        query.passed = false; 
    } else {
        query.reviewed = false;
    }

    return db.collection("titles")
        .find(query)
        .toArray();
}

module.exports.setReview = (id, value) => {
    if (!ObjectID.isValid(id)) {
        return Promise.reject(new Error("Not valid ObjectID."));
    }

    if (!validator.doBoolean(value)) {
        return Promise.reject(new Error('400'));
    }

    let update = {
        $set: {
            passed: value,
            reviewed: true
        }
    };

    return db.collection("titles").update({
        _id: ObjectID(id)
    }, update);
}