const ObjectID = require("mongodb").ObjectID;
const getdb = require("../db");

const sessionHelper = require("../helpers/session.helper");
const validator = require("../validators/session.validator");

let db = null; 
getdb.then(database => db = database); 

module.exports.find = (id) => {
    return db.collection("users")
        .findOne({ _id: ObjectID(id) });
}

module.exports.loginFind = (query) => {
    if (!validator.doLogin(query))
        return Promise.reject(new Error("400"));
        
    return db.collection("users")
            .findOne({ name: query.name });
}

module.exports.newUser = (user) => {
    if (!validator.doJoin(user))
        return Promise.reject(new Error("400"));

    let session = null;

    return sessionHelper.encryptPassword(user.password)
        .then(hash => {
            user.password = hash;
            user.priority = 5;
            return db.collection("users").insertOne(user);
        })
        .then(response => {
            if (response.ops[0]) {
                session = {
                    _id: response.ops[0]._id,
                    name: response.ops[0].name
                };
                
                return Promise.resolve();
            }

            else 
                return Promise.reject(new Error('500'))
        })
        .then(() => sessionHelper.tokenSign(session._id))
        .then(token => Promise.resolve({ session, token }))
}

module.exports.isUnique = (query) => {
    return db.collection("users").findOne(query)
        .then(response => {
            if (response) {
                return Promise.resolve({ 
                    unique: false, 
                    field: Object.keys(query)[0] 
                    // "field" it's either name or email
                });
            } else { 
                return Promise.resolve({ 
                    unique: true, 
                    field: Object.keys(query)[0]
                });
            }
        });
}