const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');

const saltRounds = 5;

module.exports.tokenVerify = (token) => {
    return new Promise((resolve, reject) => {
        if (!token)
            reject(new Error("Token undefined."));

        jwt.verify(token, "strongsecret", (err, decoded) => {
            if (err) 
                reject(err);
            resolve(decoded.data);
        });
    });
}

module.exports.tokenSign = (id) => {
    return jwt.sign({
            data: id 
           }, "strongsecret");
};

module.exports.encryptPassword = (password) => {
    return bcrypt.hash(password, saltRounds);
}

module.exports.comparePassword = (hash, password) => {
    return bcrypt.compare(password, hash);
}

