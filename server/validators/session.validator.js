function name(name) { 
    let re = /^([\w.!?_-]){2,30}$/;
    return re.test(name);
}

function email(email) {
    // e-mails are validated by sending a confirmation link 
    if (email && email.length < 255)
        return true; 
    return false;
}

function password(password) {
    let re = /^([\S]){5,}$/;
    if (re.test(password)) 
        return true;

    return false;
}

function existingKeyChecker(user) {
    for (let i = 0; i < Object.keys(user).length; i++) {
        let key = Object.keys(user)[i];    
        switch(key) {
            case "name": 
                if (!name(user.name))
                    return false;
                break;
            case "email": 
                if (!email(user.email))
                    return false;
                break; 
            case "password":
                if (!password(user.password))
                    return false;
                break;
            default: 
                break;
        }
    }

    return true;
}

module.exports.doLogin = function(user) {
    if (!user.name || !user.password)
        return false;

    if (!existingKeyChecker(user))
        return false;
    return true;
}

module.exports.doJoin = function(user) {
    if (!user.name || !user.password || !user.email)
        return false;

    if (!existingKeyChecker(user))
        return false;
    return true;
}